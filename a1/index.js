console.log("Hello World");

let trainer = {
    name: "Ash Pedreiro",
    age: 10,
    pokemon: ["Pikachu", "Charmander", "Squirtle", "Bulbasaur"],
    friends: ["Brock", "Misty", "Prof. Oak"],
    talk: function() {
        console.log("Pikachu! I choose you!");
    },
};
console.log(trainer);
console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of bracket notation:")
console.log(trainer["pokemon"]);
console.log("Result of talk method:")
trainer.talk();

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level * 1.5;
    this.tackle = function(opponent) {
        console.log(this.name + " tackled " + opponent.name);

        opponent.health -= this.attack

        console.log(opponent.name + " health is now reduced to " + opponent.health);

        if (opponent.health <= 0) {
            opponent.faint();
            console.log(opponent.name + " health is now reduced to 0.");
        }
    };
    this.faint = function() {
        console.log(this.name + " fainted.");
    }
}

let psyduck = new Pokemon ("Psyduck", 70);
console.log(psyduck)

let slowpoke = new Pokemon ("Slowpoke", 66);
console.log(slowpoke);

let snorlax = new Pokemon ("Snorlax", 99);
console.log(snorlax);

let jynx = new Pokemon ("Jynx", 50);
console.log(jynx);

psyduck.tackle(slowpoke);
snorlax.tackle(jynx);